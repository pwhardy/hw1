####  My name is Parker Hardy

This is my submission for homework1. 
I would like to include one link: [name your link](url of your link)

I would also like to include some facts in a table:

|   Item        |          Info            |
|---------------|--------------------------|
| home town     | San Antonio, TX|
| favorite song | Green Grass and High Tides by: The Outlaws |
| <anything else> | I am a huge Ole Miss baseball fan
